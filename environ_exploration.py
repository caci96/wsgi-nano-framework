from wsgiref.simple_server import make_server

def application(environ, start_response):
    from io import StringIO
    stdout = StringIO()
    print("Hello world!", file=stdout)
    print(file=stdout)
    h = (environ.items())
    for k,v in h:
        print(k,'=',repr(v), file=stdout)
    start_response("200 OK", [('Content-Type','text/plain; charset=utf-8')])
    return [stdout.getvalue().encode("utf-8")]


with make_server('', 8000, application) as httpd:
    try:
        url = "http://127.0.0.1:8000"
        print("Hello")
        print("Started serving requests on:")
        print(url)
        import webbrowser
        webbrowser.open(url)
        httpd.serve_forever()
    except KeyboardInterrupt:
        print()
        print("GoodBye")

