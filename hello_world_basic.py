from wsgiref.simple_server import make_server

def application(environ, start_response):
    start_response("200 OK", [("Content-Type", "text/plain; charset=utf-8")])
    return ("Hello World!!".encode("utf-8"),)

with make_server('', 8000, application) as httpd:
    httpd.serve_forever()

